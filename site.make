core = 7.x
api = 2

; Feds Clubs
projects[uw_ct_feds_clubs][type] = "module"
projects[uw_ct_feds_clubs][download][type] = "git"
projects[uw_ct_feds_clubs][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_clubs.git"
projects[uw_ct_feds_clubs][download][tag] = "7.x-1.3"

; Feds Employment and Volunteer
projects[uw_ct_feds_employ_volunteer][type] = "module"
projects[uw_ct_feds_employ_volunteer][download][type] = "git"
projects[uw_ct_feds_employ_volunteer][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_employ_volunteer.git"
projects[uw_ct_feds_employ_volunteer][download][tag] = "7.x-1.4"

; Feds Events
projects[uw_ct_feds_events][type] = "module"
projects[uw_ct_feds_events][download][type] = "git"
projects[uw_ct_feds_events][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_events.git"
projects[uw_ct_feds_events][download][tag] = "7.x-1.2"

; Feds event listing header
projects[uw_ct_feds_event_listing_header][type] = "module"
projects[uw_ct_feds_event_listing_header][download][type] = "git"
projects[uw_ct_feds_event_listing_header][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_event_listing_header.git"
projects[uw_ct_feds_event_listing_header][download][tag] = "7.x-1.1"

; Feds footer
projects[uw_ct_feds_footer][type] = "module"
projects[uw_ct_feds_footer][download][type] = "git"
projects[uw_ct_feds_footer][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_footer.git"
projects[uw_ct_feds_footer][download][tag] = "7.x-1.1"

; Feds Homepage Hero
projects[uw_ct_feds_hp_hero][type] = "module"
projects[uw_ct_feds_hp_hero][download][type] = "git"
projects[uw_ct_feds_hp_hero][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_hp_hero.git"
projects[uw_ct_feds_hp_hero][download][tag] = "7.x-1.0"

; Feds Homepage Main Features
projects[uw_ct_feds_hp_main_features][type] = "module"
projects[uw_ct_feds_hp_main_features][download][type] = "git"
projects[uw_ct_feds_hp_main_features][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_hp_main_features.git"
projects[uw_ct_feds_hp_main_features][download][tag] = "7.x-1.2"

; Feds Homepage Text Features
projects[uw_ct_feds_hp_text_features][type] = "module"
projects[uw_ct_feds_hp_text_features][download][type] = "git"
projects[uw_ct_feds_hp_text_features][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_hp_text_features.git"
projects[uw_ct_feds_hp_text_features][download][tag] = "7.x-1.2"

; Feds News
projects[uw_ct_feds_news][type] = "module"
projects[uw_ct_feds_news][download][type] = "git"
projects[uw_ct_feds_news][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_news.git"
projects[uw_ct_feds_news][download][tag] = "7.x-1.8"

; Feds promo banner
projects[uw_ct_feds_promo_banner][type] = "module"
projects[uw_ct_feds_promo_banner][download][type] = "git"
projects[uw_ct_feds_promo_banner][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_promo_banner.git"
projects[uw_ct_feds_promo_banner][download][tag] = "7.x-1.0"

; Feds Services
projects[uw_ct_feds_services][type] = "module"
projects[uw_ct_feds_services][download][type] = "git"
projects[uw_ct_feds_services][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_services.git"
projects[uw_ct_feds_services][download][tag] = "7.x-1.3"

; Feds Staff Contacts
projects[uw_ct_feds_staff_contacts][type] = "module"
projects[uw_ct_feds_staff_contacts][download][type] = "git"
projects[uw_ct_feds_staff_contacts][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_staff_contacts.git"
projects[uw_ct_feds_staff_contacts][download][tag] = "7.x-1.2"

; Feds Webpage with Hero Banner
projects[uw_ct_feds_webpage_with_banner][type] = "module"
projects[uw_ct_feds_webpage_with_banner][download][type] = "git"
projects[uw_ct_feds_webpage_with_banner][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_webpage_with_banner.git"
projects[uw_ct_feds_webpage_with_banner][download][tag] = "7.x-1.0"

; Feds Writers
projects[uw_ct_feds_writers][type] = "module"
projects[uw_ct_feds_writers][download][type] = "git"
projects[uw_ct_feds_writers][download][url] = "https://git.uwaterloo.ca/feds-site/uw_ct_feds_writers.git"
projects[uw_ct_feds_writers][download][tag] = "7.x-1.0"

; Feds Club Categories
projects[uw_vocab_feds_club_categories][type] = "module"
projects[uw_vocab_feds_club_categories][download][type] = "git"
projects[uw_vocab_feds_club_categories][download][url] = "https://git.uwaterloo.ca/feds-site/uw_vocab_feds_club_categories.git"
projects[uw_vocab_feds_club_categories][download][tag] = "7.x-1.0"

; Feds Departments Vocabulary
projects[uw_vocab_feds_departments][type] = "module"
projects[uw_vocab_feds_departments][download][type] = "git"
projects[uw_vocab_feds_departments][download][url] = "https://git.uwaterloo.ca/feds-site/uw_vocab_feds_departments.git"
projects[uw_vocab_feds_departments][download][tag] = "7.x-1.0"

; Feds Employment Type Vocabulary
projects[uw_vocab_feds_employment_type][type] = "module"
projects[uw_vocab_feds_employment_type][download][type] = "git"
projects[uw_vocab_feds_employment_type][download][url] = "https://git.uwaterloo.ca/feds-site/uw_vocab_feds_employment_type.git"
projects[uw_vocab_feds_employment_type][download][tag] = "7.x-1.0"

; Feds Event Ages Vocabulary
projects[uw_vocab_feds_event_ages][type] = "module"
projects[uw_vocab_feds_event_ages][download][type] = "git"
projects[uw_vocab_feds_event_ages][download][url] = "https://git.uwaterloo.ca/feds-site/uw_vocab_feds_event_ages.git"
projects[uw_vocab_feds_event_ages][download][tag] = "7.x-1.0"

; Feds Event Location
projects[uw_vocab_feds_event_location][type] = "module"
projects[uw_vocab_feds_event_location][download][type] = "git"
projects[uw_vocab_feds_event_location][download][url] = "https://git.uwaterloo.ca/feds-site/uw_vocab_feds_event_location.git"
projects[uw_vocab_feds_event_location][download][tag] = "7.x-1.0"

; Feds Event Type Vocabulary
projects[uw_vocab_feds_event_type][type] = "module"
projects[uw_vocab_feds_event_type][download][type] = "git"
projects[uw_vocab_feds_event_type][download][url] = "https://git.uwaterloo.ca/feds-site/uw_vocab_feds_event_type.git"
projects[uw_vocab_feds_event_type][download][tag] = "7.x-1.0"

; Feds Services Categories
projects[uw_vocab_feds_services_categories][type] = "module"
projects[uw_vocab_feds_services_categories][download][type] = "git"
projects[uw_vocab_feds_services_categories][download][url] = "https://git.uwaterloo.ca/feds-site/uw_vocab_feds_services_categories.git"
projects[uw_vocab_feds_services_categories][download][tag] = "7.x-1.1"

; Feds News Categories
projects[uw_vocab_feds_news_categories][type] = "module"
projects[uw_vocab_feds_news_categories][download][type] = "git"
projects[uw_vocab_feds_news_categories][download][url] = "https://git.uwaterloo.ca/feds-site/uw_vocab_feds_news_categories.git"
projects[uw_vocab_feds_news_categories][download][tag] = "7.x-1.1"

; Feds Site Controller
projects[uw_site_feds][type] = "module"
projects[uw_site_feds][download][type] = "git"
projects[uw_site_feds][download][url] = "https://git.uwaterloo.ca/feds-site/uw_site_feds.git"
projects[uw_site_feds][download][tag] = "7.x-1.2"

; Feds Responsive Configuration
projects[uw_cfg_responsive][type] = "module"
projects[uw_cfg_responsive][download][type] = "git"
projects[uw_cfg_responsive][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_responsive.git"
projects[uw_cfg_responsive][download][tag] = "7.x-1.3-feds"

; Feds Search Configuration
projects[uw_cfg_feds_search][type] = "module"
projects[uw_cfg_feds_search][download][type] = "git"
projects[uw_cfg_feds_search][download][url] = "https://git.uwaterloo.ca/feds-site/uw_cfg_feds_search.git"
projects[uw_cfg_feds_search][download][tag] = "7.x-1.2"

; Feds Metatag Configuration
projects[uw_cfg_feds_metatag][type] = "module"
projects[uw_cfg_feds_metatag][download][type] = "git"
projects[uw_cfg_feds_metatag][download][url] = "https://git.uwaterloo.ca/feds-site/uw_cfg_feds_metatag.git"
projects[uw_cfg_feds_metatag][download][tag] = "7.x-1.3"

; Feds theme
projects[feds_theme][type] = "theme"
projects[feds_theme][download][type] = "git"
projects[feds_theme][download][url] = "https://git.uwaterloo.ca/feds-site/feds_theme.git"
projects[feds_theme][download][tag] = "7.x-1.15"

; Feds Main Menu
projects[uw_nav_main_menu][type] = "module"
projects[uw_nav_main_menu][download][type] = "git"
projects[uw_nav_main_menu][download][url] = "https://git.uwaterloo.ca/feds-site/uw_nav_main_menu.git"
projects[uw_nav_main_menu][download][tag] = "7.x-1.1"

; Rabbit Hole
projects[rabbit_hole][type] = "module"
projects[rabbit_hole][download][type] = "git"
projects[rabbit_hole][download][url] = "https://git.uwaterloo.ca/feds-site/rabbit_hole.git"
projects[rabbit_hole][download][tag] = "7.x-2.25"

; Rabbit Hole Config
projects[uw_cfg_rabbit_hole][type] = "module"
projects[uw_cfg_rabbit_hole][download][type] = "git"
projects[uw_cfg_rabbit_hole][download][url] = "https://git.uwaterloo.ca/feds-site/uw_cfg_rabbit_hole.git"
projects[uw_cfg_rabbit_hole][download][tag] = "7.x-1.0"

; Temporary Modules to fix Responsive Menu below (2 module an theme)
; Responsive config
projects[uw_cfg_responsive][type] = "module"
projects[uw_cfg_responsive][download][type] = "git"
projects[uw_cfg_responsive][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_responsive.git"
projects[uw_cfg_responsive][download][tag] = "7.x-1.6"
projects[uw_cfg_responsive][subdir] = "features"

; uWaterloo FDSU Theme Responsive
projects[uw_fdsu_theme_resp][type] = "theme"
projects[uw_fdsu_theme_resp][download][type] = "git"
projects[uw_fdsu_theme_resp][download][url] = "https://git.uwaterloo.ca/wcms/uw_fdsu_theme_resp.git"
projects[uw_fdsu_theme_resp][download][tag] = "7.x-1.33"

; Responsive Menu Combined
projects[responsive_menu_combined][type] = "module"
projects[responsive_menu_combined][download][type] = "git"
projects[responsive_menu_combined][download][url] = "https://git.uwaterloo.ca/drupal-org/responsive_menu_combined.git"
projects[responsive_menu_combined][download][tag] = "7.x-1.0-alpha3-uw_wcms2"
projects[responsive_menu_combined][subdir] = "contrib"

